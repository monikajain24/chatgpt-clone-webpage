// JavaScript to handle adding new chats
const newChatButton = document.getElementById("new-chat-button");
const listOfChats = document.getElementById("list-of-chats");

newChatButton.addEventListener("click", () => {
    // Create a new chat item
    const chatItem = document.createElement("div");
    chatItem.classList.add("single-chat-item");

    const chatIcon = document.createElement("div");
    chatIcon.classList.add("chat-icon");

    const chatTitleDiv = document.createElement("div");
    chatTitleDiv.classList.add("chat-title");

    // Set a dummy chat title
    chatTitleDiv.textContent = "Dummy Chat"; 

    const deleteButton = document.createElement("button");
    deleteButton.classList.add("delete-button");
    deleteButton.textContent = "Delete";
    deleteButton.addEventListener("click", () => {
        // Prompt the user to confirm deletion
        const confirmDelete = confirm("Are you sure you want to delete this chat?");
        if (confirmDelete) {
            listOfChats.removeChild(chatItem);
        }
    });

    chatItem.appendChild(chatIcon);
    chatItem.appendChild(chatTitleDiv);
    chatItem.appendChild(deleteButton);

    // Add the new chat to the list_of_chats section
    listOfChats.appendChild(chatItem);
});

// JavaScript to handle sending messages (unchanged)
const sendButton = document.getElementById("send-button");
const searchInput = document.getElementById("search-input");
const chatMessages = document.getElementById("chat-messages"); // Assuming you have a container for chat messages

sendButton.addEventListener("click", () => {
    const messageText = searchInput.value;
    if (messageText) {
        // Create a new message element and append it to the chatMessages container
        const messageElement = document.createElement("div");
        messageElement.classList.add("message");
        messageElement.textContent = messageText;

        chatMessages.appendChild(messageElement);

        // Clear the input field after sending
        searchInput.value = "";
    }
});
// JavaScript to handle closing the left pane
const closeSidebarButton = document.getElementById("close-sidebar-button");
const leftPane = document.getElementById("left-pane");

closeSidebarButton.addEventListener("click", () => {
    leftPane.classList.toggle("hidden");
});

